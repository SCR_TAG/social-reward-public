package de.uni_leipzig.lfe.scr_tab.socialreward.testing;

import android.util.Pair;

import java.util.HashMap;
import java.util.Map;

import de.uni_leipzig.lfe.scr_tab.socialreward.block.Side;
import de.uni_leipzig.lfe.scr_tab.socialreward.block.Trial;

public class BlockExecutionRecorder {
    private Map<Trial, Pair<Long, Long>> timeMap = new HashMap<>();
    private Map<Trial, Side> sideMap = new HashMap<>();

    public void start(Trial trial) {
        timeMap.put(trial, Pair.create(System.currentTimeMillis(), 0L));
    }

    public void stop(Trial trial, Side side) {
        timeMap.put(trial, Pair.create(timeMap.get(trial).first, System.currentTimeMillis()));
        sideMap.put(trial, side);
    }

    public long getStart(Trial trial) {
        return timeMap.get(trial).first;
    }

    public long getEnd(Trial trial) {
        return timeMap.get(trial).second;
    }

    public Side getSide(Trial trial) {
        return sideMap.get(trial);
    }

    public long getDuration(Trial trial) {
        Pair<Long, Long> pair = timeMap.get(trial);
        return pair.second - pair.first;
    }
}
