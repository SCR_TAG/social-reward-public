package de.uni_leipzig.lfe.scr_tab.socialreward;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import de.uni_leipzig.lfe.scr_tab.socialreward.block.BlockRecord;
import de.uni_leipzig.lfe.scr_tab.socialreward.block.BlockRecordDao;
import de.uni_leipzig.lfe.scr_tab.socialreward.culture.Culture;
import de.uni_leipzig.lfe.scr_tab.socialreward.culture.CultureDao;

@Database(entities = {Culture.class, BlockRecord.class}, version = 2, exportSchema = true)
public abstract class AppDatabase extends RoomDatabase {
    public abstract CultureDao cultureDao();

    public abstract BlockRecordDao blockRecordDao();
}
