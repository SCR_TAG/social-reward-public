package de.uni_leipzig.lfe.scr_tab.socialreward.configurationmode;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import de.uni_leipzig.lfe.scr_tab.socialreward.PrefsManager;
import de.uni_leipzig.lfe.scr_tab.socialreward.R;

public class ConfigurationModeHandler {
    private class ConfigurationModeSelectListener implements DialogInterface.OnClickListener {
        private ConfigurationMode configurationMode;

        ConfigurationModeSelectListener(ConfigurationMode configurationMode) {
            this.configurationMode = configurationMode;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            new PrefsManager(((AlertDialog) dialog).getContext()).setConfigurationMode(configurationMode);
        }
    }

    public void assertConfigurationMode(Context context) {
        if (new PrefsManager(context).getConfigurationMode() == null) {
            new AlertDialog.Builder(context)
                    .setTitle(R.string.select_configuration_mode_title)
                    .setMessage(R.string.select_configuration_mode_msg)
                    .setPositiveButton(R.string.yes, new ConfigurationModeSelectListener(ConfigurationMode.CONFIGURATOR))
                    .setNegativeButton(R.string.no, new ConfigurationModeSelectListener(ConfigurationMode.FOLLOWER))
                    .show();
        }
    }
}
