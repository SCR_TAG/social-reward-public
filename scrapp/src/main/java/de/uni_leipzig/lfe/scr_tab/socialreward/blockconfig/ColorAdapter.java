package de.uni_leipzig.lfe.scr_tab.socialreward.blockconfig;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import de.uni_leipzig.lfe.scr_tab.socialreward.R;

class ColorAdapter extends ArrayAdapter<Integer> {
    ColorAdapter(@NonNull Context context) {
        super(context, R.layout.colorspinner_item);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Nullable
    @Override
    public Integer getItem(int position) {
        switch (position) {
            case 0:
                return getContext().getResources().getColor(R.color.socialStimulusColor1);
            case 1:
                return getContext().getResources().getColor(R.color.socialStimulusColor2);
        }
        return 0;
    }

    @Override
    public long getItemId(int position) {
        switch (position) {
            case 0:
                return R.color.socialStimulusColor1;
            case 1:
                return R.color.socialStimulusColor2;
        }
        return 0;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return setColoredView(position, (TextView) super.getView(position, convertView, parent));
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return setColoredView(position, (TextView) super.getDropDownView(position, convertView, parent));
    }

    private View setColoredView(int position, TextView textView) {
        textView.setText("");
        textView.setBackgroundColor(getItem(position));
        return textView;

    }
}
