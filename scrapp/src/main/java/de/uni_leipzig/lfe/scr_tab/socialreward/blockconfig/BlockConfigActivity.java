package de.uni_leipzig.lfe.scr_tab.socialreward.blockconfig;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import de.uni_leipzig.lfe.scr_tab.socialreward.AppDatabase;
import de.uni_leipzig.lfe.scr_tab.socialreward.BuildConfig;
import de.uni_leipzig.lfe.scr_tab.socialreward.R;
import de.uni_leipzig.lfe.scr_tab.socialreward.ScRApplication;
import de.uni_leipzig.lfe.scr_tab.socialreward.block.BlockPlannerTask;
import de.uni_leipzig.lfe.scr_tab.socialreward.block.BlockRecordExportTask;
import de.uni_leipzig.lfe.scr_tab.socialreward.configurationmode.ConfigurationModeHandler;
import de.uni_leipzig.lfe.scr_tab.socialreward.culture.NewCultureActivity;

public class BlockConfigActivity extends AppCompatActivity {
    private static final int NEW_CULTURE_REQUESTCODE = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_config);

        if (BuildConfig.MULTIPLE_DEVICES) {
            new ConfigurationModeHandler().assertConfigurationMode(this);
        }

        findViewById(R.id.new_culture_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newCultureIntent = new Intent(BlockConfigActivity.this, NewCultureActivity.class);
                startActivityForResult(newCultureIntent, NEW_CULTURE_REQUESTCODE);
            }
        });

        setupCultureSpinner();
        setSocialStimuliSpinner();

        findViewById(R.id.start_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long cultureId = ((Spinner) findViewById(R.id.culture_spinner)).getSelectedItemId();
                int socialStimuliColor = (int) ((Spinner) findViewById(R.id.social_stimuli_spinner)).getSelectedItemId();
                boolean scrambleImages = ((CheckBox) findViewById(R.id.scamble_images_checkbox)).isChecked();
                boolean showMatureImages = ((CheckBox) findViewById(R.id.show_mature_images_checkbox)).isChecked();

                String testPersionId = ((EditText) findViewById(R.id.test_person_id_edittext)).getText().toString();
                int testPersonSex = (int) ((Spinner) findViewById(R.id.test_person_sex)).getSelectedItemPosition();
                String ageString = ((EditText) findViewById(R.id.test_person_age_edittext)).getText().toString();

                if (TextUtils.isEmpty(testPersionId)) {
                    Toast.makeText(BlockConfigActivity.this, R.string.no_test_person_id,
                            Toast.LENGTH_SHORT)
                            .show();
                } else if (TextUtils.isEmpty(ageString)) {
                    Toast.makeText(BlockConfigActivity.this, R.string.no_test_person_age,
                            Toast.LENGTH_SHORT)
                            .show();
                } else {
                    int testPersonAge = Integer.parseInt(ageString);
                    BlockConfiguration blockConfiguration = new BlockConfiguration(cultureId,
                            socialStimuliColor, scrambleImages, showMatureImages,
                            testPersionId, testPersonSex, testPersonAge);

                    new BlockPlannerTask((ScRApplication) getApplication()).execute(blockConfiguration);
                    // TODO: show spinner ...
                }
            }
        });

        findViewById(R.id.export_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BlockRecordExportTask((ScRApplication) getApplication()).execute();
            }
        });

        findViewById(R.id.export_current_culture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long cultureId = ((Spinner) findViewById(R.id.culture_spinner)).getSelectedItemId();
                new BlockRecordExportTask((ScRApplication) getApplication()).execute(cultureId);
            }
        });
    }

    private void setSocialStimuliSpinner() {
        ((Spinner) findViewById(R.id.social_stimuli_spinner)).setAdapter(new ColorAdapter(this));
    }

    private void setupCultureSpinner() {
        new CultureQueryTask(this, (Spinner) findViewById(R.id.culture_spinner)).execute();
    }

    private static class CultureQueryTask extends AsyncTask<Void, Void, Cursor> {
        private Spinner cultureSpinner;
        private Activity activity;

        CultureQueryTask(Activity activity, Spinner cultureSpinner) {
            this.activity = activity;
            this.cultureSpinner = cultureSpinner;
        }

        @Override
        protected Cursor doInBackground(Void... voids) {
            AppDatabase appDatabase = ((ScRApplication) activity.getApplication()).getAppDatabase();
            return appDatabase.cultureDao().getAllCultures();
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            CursorAdapter culturesAdaptor = new CultureAdapter(activity, cursor);
            cultureSpinner.setAdapter(culturesAdaptor);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == NEW_CULTURE_REQUESTCODE) {
            setupCultureSpinner();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
