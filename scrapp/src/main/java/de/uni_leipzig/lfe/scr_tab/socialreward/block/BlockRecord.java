package de.uni_leipzig.lfe.scr_tab.socialreward.block;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.ArrayList;

import de.uni_leipzig.lfe.scr_tab.socialreward.util.TrialRecordConverters;

@Entity
public class BlockRecord {
    @PrimaryKey(autoGenerate = true)
    private long _id;

    private String culture;
    private String subjectId;
    private String subjectSex;
    private int subjectAge;
    private long start;
    private long end;
    private int socialStimulusColor;
    private boolean showMatureImages;
    private boolean scrambleImages;


    public void set_id(long _id) {
        this._id = _id;
    }

    @TypeConverters({TrialRecordConverters.class})
    private ArrayList<TrialRecord> testRecords;

    @TypeConverters({TrialRecordConverters.class})
    private ArrayList<TrialRecord> trainingRecords;

    @TypeConverters({TrialRecordConverters.class})
    private ArrayList<TrialRecord> familiarizationRecords;

    public void setCulture(String culture) {
        this.culture = culture;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public void setSubjectSex(String subjectSex) {
        this.subjectSex = subjectSex;
    }

    public void setSubjectAge(int subjectAge) {
        this.subjectAge = subjectAge;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public void setSocialStimulusColor(int socialStimulusColor) {
        this.socialStimulusColor = socialStimulusColor;
    }

    public void setShowMatureImages(boolean showMatureImages) {
        this.showMatureImages = showMatureImages;
    }

    public void setScrambleImages(boolean scrambleImages) {
        this.scrambleImages = scrambleImages;
    }

    public void setTestRecords(ArrayList<TrialRecord> testRecords) {
        this.testRecords = testRecords;
    }

    public void setTrainingRecords(ArrayList<TrialRecord> trainingRecords) {
        this.trainingRecords = trainingRecords;
    }

    public void setFamiliarizationRecords(ArrayList<TrialRecord> familiarizationRecords) {
        this.familiarizationRecords = familiarizationRecords;
    }

    public long get_id() {
        return _id;
    }

    public String getCulture() {
        return culture;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public String getSubjectSex() {
        return subjectSex;
    }

    public int getSubjectAge() {
        return subjectAge;
    }

    public long getStart() {
        return start;
    }

    public long getEnd() {
        return end;
    }

    public int getSocialStimulusColor() {
        return socialStimulusColor;
    }

    public boolean isShowMatureImages() {
        return showMatureImages;
    }

    public boolean isScrambleImages() {
        return scrambleImages;
    }

    public ArrayList<TrialRecord> getTestRecords() {
        return testRecords;
    }

    public ArrayList<TrialRecord> getTrainingRecords() {
        return trainingRecords;
    }

    public ArrayList<TrialRecord> getFamiliarizationRecords() {
        return familiarizationRecords;
    }
}
