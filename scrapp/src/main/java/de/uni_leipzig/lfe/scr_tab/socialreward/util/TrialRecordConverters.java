package de.uni_leipzig.lfe.scr_tab.socialreward.util;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import de.uni_leipzig.lfe.scr_tab.socialreward.block.TrialRecord;

public class TrialRecordConverters {
    @TypeConverter
    public static ArrayList<TrialRecord> fromString(String value) {
        Type listType = new TypeToken<ArrayList<TrialRecord>>() {
        }.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromArrayList(ArrayList<TrialRecord> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }
}

