package de.uni_leipzig.lfe.scr_tab.socialreward.balance;

import java.util.List;
import java.util.Random;

public class BalanceFactory<T> {
    public Balancer<T> createBalancer(List<T> values, int totalCount) {
        if (values.size() < totalCount) {
            if (values.size() == 2) {
                return new CounterBalancer<>(values, totalCount, new Random());
            }
            throw new RuntimeException("No CounterBalancer defined for more than 2 values!");
        } else {
            return new RandomBalancer<>(values, totalCount);
        }
    }
}
