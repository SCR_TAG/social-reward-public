package de.uni_leipzig.lfe.scr_tab.socialreward.block;

public enum StimulusType {
    NONE,
    SOCIAL,
    NONSOCIAL
}
