package de.uni_leipzig.lfe.scr_tab.socialreward.block;

public enum Side {
    LEFT,
    RIGHT
}
