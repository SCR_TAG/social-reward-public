package de.uni_leipzig.lfe.scr_tab.socialreward.block;

public class Trial {
    private TrialConfiguration left;
    private TrialConfiguration right;
    private long si;
    private long isi;
    private int flag;

    Trial(TrialConfiguration left, TrialConfiguration right, long si, long isi) {
        this(left, right, si, isi, 0);
    }

    Trial(TrialConfiguration left, TrialConfiguration right, long si, long isi, int flag) {
        this.left = left;
        this.right = right;
        this.si = si;
        this.isi = isi;
        this.flag = flag;
    }

    public TrialConfiguration getLeft() {
        return left;
    }

    public TrialConfiguration getRight() {
        return right;
    }

    public long getSi() {
        return si;
    }

    public long getIsi() {
        return isi;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }
}
