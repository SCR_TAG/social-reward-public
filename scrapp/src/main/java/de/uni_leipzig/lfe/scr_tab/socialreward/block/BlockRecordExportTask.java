package de.uni_leipzig.lfe.scr_tab.socialreward.block;

import android.os.AsyncTask;
import android.os.Environment;

import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.uni_leipzig.lfe.scr_tab.socialreward.AppDatabase;
import de.uni_leipzig.lfe.scr_tab.socialreward.ScRApplication;
import de.uni_leipzig.lfe.scr_tab.socialreward.culture.Culture;
import de.uni_leipzig.lfe.scr_tab.socialreward.culture.CultureDao;

public class BlockRecordExportTask extends AsyncTask<Long, Void, Void> {
    private static final String HEADER[] = {"_id", "culture", "subjectId", "sex", "age", "start", "end",
            "socialStimulusColor", "showMatureImages", "scrambleImages",
            "training_1_stimulus", "training_1_reactiontime", "training_1_si_time", "training_1_isi_time", "training_1_side",
            "training_2_stimulus", "training_2_reactiontime", "training_2_si_time", "training_2_isi_time", "training_2_side",
            "training_3_stimulus", "training_3_reactiontime", "training_3_si_time", "training_3_isi_time", "training_3_side",
            "training_4_stimulus", "training_4_reactiontime", "training_4_si_time", "training_4_isi_time", "training_4_side",
            "1_stimulus", "1_reactiontime", "1_si_time", "1_isi_time", "1_side",
            "2_stimulus", "2_reactiontime", "2_si_time", "2_isi_time", "2_side",
            "3_stimulus", "3_reactiontime", "3_si_time", "3_isi_time", "3_side",
            "4_stimulus", "4_reactiontime", "4_si_time", "4_isi_time", "4_side",
            "5_stimulus", "5_reactiontime", "5_si_time", "5_isi_time", "5_side",
            "6_stimulus", "6_reactiontime", "6_si_time", "6_isi_time", "6_side",
            "7_stimulus", "7_reactiontime", "7_si_time", "7_isi_time", "7_side",
            "8_stimulus", "8_reactiontime", "8_si_time", "8_isi_time", "8_side"
    };

    private final BlockRecordDao blockRecordDao;
    private final CultureDao cultureDao;

    public BlockRecordExportTask(ScRApplication application) {
        AppDatabase appDatabase = application.getAppDatabase();
        blockRecordDao = appDatabase.blockRecordDao();
        cultureDao = appDatabase.cultureDao();
    }

    @Override
    protected Void doInBackground(Long... cultureIds) {
        if (cultureIds == null || cultureIds.length == 0) {
            File file = getExportFile("all");
            export(file, blockRecordDao.getAllRecords());
        } else {
            for (long cId : cultureIds) {
                Culture culture = cultureDao.getCultureById(cId);
                String shortName = culture.getShortName();
                File file = getExportFile(shortName);
                export(file, blockRecordDao.getRecordsByCulture(shortName));
            }
        }

        return null;
    }

    private File getExportFile(String culture) {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        File pileDir = new File(externalStorageDirectory, "SocialReward");
        boolean createdDir = pileDir.mkdir();
        String fileName = String.format(Locale.GERMAN, "SocialReward_%1$s_%2$s.csv", culture,
                new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.GERMAN).format(new Date()));
        return new File(pileDir, fileName);
    }


    private void export(File exportFile, List<BlockRecord> records) {
        ICsvBeanWriter beanWriter = null;
        try {
            beanWriter = new CsvBeanWriter(new FileWriter(exportFile), CsvPreference.STANDARD_PREFERENCE);

            final CellProcessor[] processors = getProcessors();

            beanWriter.writeHeader(HEADER);

            for (BlockRecord record : records) {
                beanWriter.write(record, getNameMapping(), processors);
            }
            beanWriter.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (beanWriter != null) {
                try {
                    beanWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String[] getNameMapping() {
        List<String> names = new ArrayList<>();
        names.add("_id");
        names.add("culture");
        names.add("subjectId");
        names.add("subjectSex");
        names.add("subjectAge");
        names.add("start");
        names.add("end");
        names.add("socialStimulusColor");
        names.add("showMatureImages");
        names.add("scrambleImages");

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                names.add("trainingRecords");
            }
        }

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 5; j++) {
                names.add("testRecords");
            }
        }

        String[] nameMappings = new String[names.size()];
        names.toArray(nameMappings);
        return nameMappings;
    }

    private CellProcessor[] getProcessors() {
        List<CellProcessor> processors = new ArrayList<>();

        processors.add(new NotNull()); // _id;
        processors.add(new NotNull()); // culture
        processors.add(new NotNull()); // subjectId
        processors.add(new NotNull()); // sex
        processors.add(new NotNull()); // age
        processors.add(new NotNull()); // start
        processors.add(new NotNull()); // end
        processors.add(new NotNull()); // socialStimulusColor
        processors.add(new NotNull()); // showMatureImages
        processors.add(new NotNull()); // scrambleImages;

        for (int i = 0; i < 4; i++) {
            processors.add(new TrialRecordAdapter(i, "stimulus")); // training_i_stimulus
            processors.add(new TrialRecordAdapter(i, "duration")); // training_i_reactiontime
            processors.add(new TrialRecordAdapter(i, "si_time")); // training_i_si_time
            processors.add(new TrialRecordAdapter(i, "isi_time")); // training_i_isi_time
            processors.add(new TrialRecordAdapter(i, "side")); // training_i_side
        }

        for (int i = 0; i < 8; i++) {
            processors.add(new TrialRecordAdapter(i, "stimulus")); // i_stimulus
            processors.add(new TrialRecordAdapter(i, "duration")); // i_duration
            processors.add(new TrialRecordAdapter(i, "si_time")); // i_si_time
            processors.add(new TrialRecordAdapter(i, "isi_time")); // i_isi_time
            processors.add(new TrialRecordAdapter(i, "side")); // i_side
        }

        CellProcessor[] processorsArr = new CellProcessor[processors.size()];
        processors.toArray(processorsArr);
        return processorsArr;
    }
}
