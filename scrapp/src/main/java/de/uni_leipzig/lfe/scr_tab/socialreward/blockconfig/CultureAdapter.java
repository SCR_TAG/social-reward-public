package de.uni_leipzig.lfe.scr_tab.socialreward.blockconfig;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

class CultureAdapter extends CursorAdapter {
    final LayoutInflater cursorInflater;

    CultureAdapter(Context context, Cursor csr) {
        super(context, csr, false);
        cursorInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return cursorInflater.inflate(android.R.layout.simple_spinner_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView textViewTitle = (TextView) view.findViewById(android.R.id.text1);
        String title = cursor.getString(cursor.getColumnIndex("longName"));
        textViewTitle.setText(title);
    }
}
