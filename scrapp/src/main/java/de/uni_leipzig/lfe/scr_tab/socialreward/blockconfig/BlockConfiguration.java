package de.uni_leipzig.lfe.scr_tab.socialreward.blockconfig;

public class BlockConfiguration {
    private final long cultureId;
    private final int socialStimuliColorId;
    private final boolean scrambleImages;
    private final boolean showMatureImages;
    private final String testPersionId;
    private final int testPersonSex;
    private final int testPersonAge;

    BlockConfiguration(long cultureId, int socialStimuliColorId, boolean scrambleImages, boolean showMatureImages, String testPersionId, int testPersonSex, int testPersonAge) {
        this.cultureId = cultureId;
        this.socialStimuliColorId = socialStimuliColorId;
        this.scrambleImages = scrambleImages;
        this.showMatureImages = showMatureImages;
        this.testPersionId = testPersionId;
        this.testPersonSex = testPersonSex;
        this.testPersonAge = testPersonAge;
    }

    public long getCultureId() {
        return cultureId;
    }

    public int getSocialStimuliColorId() {
        return socialStimuliColorId;
    }

    public boolean isScrambleImages() {
        return scrambleImages;
    }

    public boolean showMatureImages() {
        return showMatureImages;
    }

    public String getTestPersionId() {
        return testPersionId;
    }

    public int getTestPersonSex() {
        return testPersonSex;
    }

    public int getTestPersonAge() {
        return testPersonAge;
    }
}
