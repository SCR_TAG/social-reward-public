package de.uni_leipzig.lfe.scr_tab.socialreward;

import android.content.Context;
import android.content.SharedPreferences;

import de.uni_leipzig.lfe.scr_tab.socialreward.configurationmode.ConfigurationMode;

public class PrefsManager {
    private static final String PREFS_FILENAME = "prefs";
    private static final String CONFIGURATION_MODE = "CONFIGURATION_MODE";

    private final SharedPreferences prefs;

    public PrefsManager(Context context) {
        prefs = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE);
    }

    public void setConfigurationMode(ConfigurationMode configurationMode) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CONFIGURATION_MODE, configurationMode.name());
        editor.commit();
    }

    public ConfigurationMode getConfigurationMode() {
        String configurationMode = prefs.getString(CONFIGURATION_MODE, null);
        return configurationMode == null ? null : ConfigurationMode.valueOf(configurationMode);
    }
}
