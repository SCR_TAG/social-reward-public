package de.uni_leipzig.lfe.scr_tab.socialreward.culture;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Culture {
    @PrimaryKey(autoGenerate = true)
    private long _id;

    @NonNull
    private final String shortName;

    @NonNull
    private final String longName;
    @NonNull
    private final String youthPictureStimulus;
    @NonNull
    private final String scrambledYouthPictureStimulus;
    @NonNull
    private final String maturePictureStimulus;
    @NonNull
    private final String scrambledMaturePictureStimulus;
    @NonNull
    private final String nonsocialStimulus;
    @NonNull
    private final String socialTrainingStimulus;
    @NonNull
    private final String nonSocialTrainingStimulus;
    @NonNull
    private final String familiarizationStimulus;

    public Culture(@NonNull String shortName, @NonNull String longName,
                   @NonNull String youthPictureStimulus, @NonNull String scrambledYouthPictureStimulus,
                   @NonNull String maturePictureStimulus, @NonNull String scrambledMaturePictureStimulus,
                   @NonNull String nonsocialStimulus,
                   @NonNull String socialTrainingStimulus, @NonNull String nonSocialTrainingStimulus,
                   @NonNull String familiarizationStimulus) {
        this.shortName = shortName;
        this.longName = longName;
        this.youthPictureStimulus = youthPictureStimulus;
        this.scrambledYouthPictureStimulus = scrambledYouthPictureStimulus;
        this.maturePictureStimulus = maturePictureStimulus;
        this.scrambledMaturePictureStimulus = scrambledMaturePictureStimulus;
        this.nonsocialStimulus = nonsocialStimulus;
        this.socialTrainingStimulus = socialTrainingStimulus;
        this.nonSocialTrainingStimulus = nonSocialTrainingStimulus;
        this.familiarizationStimulus = familiarizationStimulus;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public long get_id() {
        return _id;
    }

    public String getShortName() {
        return shortName;
    }

    public String getLongName() {
        return longName;
    }

    public String getYouthPictureStimulus() {
        return youthPictureStimulus;
    }

    public String getMaturePictureStimulus() {
        return maturePictureStimulus;
    }

    @NonNull
    public String getScrambledYouthPictureStimulus() {
        return scrambledYouthPictureStimulus;
    }

    @NonNull
    public String getScrambledMaturePictureStimulus() {
        return scrambledMaturePictureStimulus;
    }

    @NonNull
    public String getNonsocialStimulus() {
        return nonsocialStimulus;
    }

    @NonNull
    public String getSocialTrainingStimulus() {
        return socialTrainingStimulus;
    }

    @NonNull
    public String getNonSocialTrainingStimulus() {
        return nonSocialTrainingStimulus;
    }

    public String getFamiliarizationStimulus() {
        return familiarizationStimulus;
    }
}
