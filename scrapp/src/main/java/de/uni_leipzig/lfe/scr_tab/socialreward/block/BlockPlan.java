package de.uni_leipzig.lfe.scr_tab.socialreward.block;

import java.util.LinkedList;
import java.util.Queue;

import de.uni_leipzig.lfe.scr_tab.socialreward.blockconfig.BlockConfiguration;

public class BlockPlan {

    private final BlockConfiguration blockConfiguration;
    private final Queue<Trial> trials;

    BlockPlan(BlockConfiguration blockConfiguration, Queue<Trial> trials) {
        this.blockConfiguration = blockConfiguration;
        this.trials = trials;
    }

    public LinkedList<Trial> getTrials() {
        // return local copy, as we need the trials later on...
        LinkedList<Trial> localTrials = new LinkedList<>();
        localTrials.addAll(this.trials);
        return localTrials;
    }

    public BlockConfiguration getBlockConfiguration() {
        return blockConfiguration;
    }
}
