package de.uni_leipzig.lfe.scr_tab.socialreward.block;

import org.supercsv.cellprocessor.CellProcessorAdaptor;
import org.supercsv.util.CsvContext;

import java.util.List;

class TrialRecordAdapter extends CellProcessorAdaptor {
    private int index;
    private String field;

    public TrialRecordAdapter(int index, String field) {
        super();
        this.index = index;
        this.field = field;
    }

    @Override
    public <T> T execute(Object value, CsvContext context) {
        validateInputNotNull(value, context);

        List<TrialRecord> trialRecords = (List<TrialRecord>) value;
        TrialRecord record = trialRecords.get(index);

        if ("stimulus".equals(field)) {
            return (T) record.getStimulusType().toString();
        } else if ("duration".equals(field)) {
            return (T) String.valueOf(record.getDuration());
        } else if ("si_time".equals(field)) {
            return (T) String.valueOf(record.getSiTime());
        } else if ("isi_time".equals(field)) {
            return (T) String.valueOf(record.getIsiTime());
        } else if ("side".equals(field)) {
            return (T) record.getSide().toString();
        }
        return (T) "";
    }
}
