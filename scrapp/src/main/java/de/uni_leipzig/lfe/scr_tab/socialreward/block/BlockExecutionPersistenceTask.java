package de.uni_leipzig.lfe.scr_tab.socialreward.block;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Pair;

import java.util.ArrayList;
import java.util.LinkedList;

import de.uni_leipzig.lfe.scr_tab.socialreward.R;
import de.uni_leipzig.lfe.scr_tab.socialreward.ScRApplication;
import de.uni_leipzig.lfe.scr_tab.socialreward.blockconfig.BlockConfiguration;
import de.uni_leipzig.lfe.scr_tab.socialreward.culture.Culture;
import de.uni_leipzig.lfe.scr_tab.socialreward.testing.BlockExecutionRecorder;
import de.uni_leipzig.lfe.scr_tab.socialreward.testing.LocalTrialActivity;

public class BlockExecutionPersistenceTask extends AsyncTask<Pair<BlockPlan, BlockExecutionRecorder>, Void, Long> {

    private ScRApplication application;
    private LocalTrialActivity.OnBlockRecordStoredListener onBlockRecordStoredListener;

    public BlockExecutionPersistenceTask(ScRApplication application,
                                         LocalTrialActivity.OnBlockRecordStoredListener onBlockRecordStoredListener) {
        this.application = application;
        this.onBlockRecordStoredListener = onBlockRecordStoredListener;
    }

    @Override
    protected Long doInBackground(Pair<BlockPlan, BlockExecutionRecorder>... pairs) {
        BlockPlan blockPlan = pairs[0].first;
        BlockExecutionRecorder blockExecutionRecorder = pairs[0].second;
        BlockRecordDao blockRecordDao = application.getAppDatabase().blockRecordDao();
        BlockRecord blockRecord = createBlockRecord(blockPlan, blockExecutionRecorder);
        return blockRecordDao.insertBlockRecord(blockRecord);
    }

    @Override
    protected void onPostExecute(Long blockRecordId) {
        super.onPostExecute(blockRecordId);
        onBlockRecordStoredListener.onBlockRecordStored(blockRecordId);
    }

    private BlockRecord createBlockRecord(BlockPlan blockPlan, BlockExecutionRecorder blockExecutionRecorder) {
        Resources resources = application.getResources();

        BlockConfiguration blockConfiguration = blockPlan.getBlockConfiguration();
        LinkedList<Trial> trials = blockPlan.getTrials();

        BlockRecord blockRecord = new BlockRecord();

        blockRecord.setSubjectId(blockConfiguration.getTestPersionId());

        Culture culture = application.getAppDatabase().cultureDao().getCultureById(blockConfiguration.getCultureId());
        blockRecord.setCulture(culture.getShortName());

        blockRecord.setSubjectSex(resources.getStringArray(R.array.sexes)[blockConfiguration.getTestPersonSex()]);
        blockRecord.setSubjectAge(blockConfiguration.getTestPersonAge());
        blockRecord.setStart(blockExecutionRecorder.getStart(trials.getFirst()));
        blockRecord.setEnd(blockExecutionRecorder.getEnd(trials.getLast()));
        blockRecord.setSocialStimulusColor(resources.getColor(blockConfiguration.getSocialStimuliColorId()));
        blockRecord.setShowMatureImages(blockConfiguration.showMatureImages());
        blockRecord.setScrambleImages(blockConfiguration.isScrambleImages());

        ArrayList<TrialRecord> familiarizationRecords = new ArrayList<>();
        ArrayList<TrialRecord> trainingRecords = new ArrayList<>();
        ArrayList<TrialRecord> testRecords = new ArrayList<>();

        Trial trial;
        do {
            trial = trials.pop();
            TrialRecord trialRecord = createTrialRecord(blockExecutionRecorder, trial);
            familiarizationRecords.add(trialRecord);

        } while (trial.getFlag() != BlockPlanner.END_OF_FAMILIARIZATION);
        blockRecord.setFamiliarizationRecords(familiarizationRecords);

        do {
            trial = trials.pop();
            TrialRecord trialRecord = createTrialRecord(blockExecutionRecorder, trial);
            trainingRecords.add(trialRecord);

        } while (trial.getFlag() != BlockPlanner.END_OF_TRAINING);
        blockRecord.setTrainingRecords(trainingRecords);

        do {
            trial = trials.pop();
            TrialRecord trialRecord = createTrialRecord(blockExecutionRecorder, trial);
            testRecords.add(trialRecord);

        } while (trial.getFlag() != BlockPlanner.END_OF_TESTING);
        blockRecord.setTestRecords(testRecords);

        return blockRecord;
    }

    @NonNull
    private TrialRecord createTrialRecord(BlockExecutionRecorder blockExecutionRecorder, Trial trial) {
        Side side = blockExecutionRecorder.getSide(trial);

        TrialRecord trialRecord = new TrialRecord();

        trialRecord.setSide(side);
        trialRecord.setStimulusType(getStimulusTypeColorFromSide(side, trial));
        trialRecord.setDuration(blockExecutionRecorder.getDuration(trial));
        trialRecord.setSiTime(trial.getSi());
        trialRecord.setIsiTime(trial.getIsi());
        return trialRecord;
    }

    private StimulusType getStimulusTypeColorFromSide(Side side, Trial trial) {
        TrialConfiguration trialConfiguration = side == Side.LEFT ? trial.getLeft() : trial.getRight();
        return trialConfiguration.getStimulusType();
    }
}
