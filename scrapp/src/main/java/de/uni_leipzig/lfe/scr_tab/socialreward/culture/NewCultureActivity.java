package de.uni_leipzig.lfe.scr_tab.socialreward.culture;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import de.uni_leipzig.lfe.scr_tab.socialreward.R;
import de.uni_leipzig.lfe.scr_tab.socialreward.ScRApplication;
import de.uni_leipzig.lfe.scr_tab.socialreward.util.UriUtils;

public class NewCultureActivity extends AppCompatActivity {

    private static final int YOUTH_STIMULUS_REQUESTCODE = 1100;
    private static final int MATURE_STIMULUS_REQUESTCODE = 1101;
    private static final int SCRAMBLED_YOUTH_STIMULUS_REQUESTCODE = 1102;
    private static final int SCRAMBLED_MATURE_STIMULUS_REQUESTCODE = 1103;
    private static final int NONSOCIAL_STIMULUS_REQUESTCODE = 1104;
    private static final int SOCIAL_TRAINING_STIMULUS_REQUESTCODE = 1105;
    private static final int NONSOCIAL_TRAINING_STIMULUS_REQUESTCODE = 1106;
    private static final int FAMILIARIZATION_STIMULUS_REQUESTCODE = 1107;
    private static final int PERMISSION_GRANT_CODE = 1110;

    private static final String MIMETYPE_FILE = "*/*";

    public static final String SHORT_NAME = "SHORT_NAME";

    private View.OnClickListener directoryListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.youth_stimulus_btn:
                    requestDirectory(YOUTH_STIMULUS_REQUESTCODE);
                    break;
                case R.id.scrambled_youth_stimulus_btn:
                    requestDirectory(SCRAMBLED_YOUTH_STIMULUS_REQUESTCODE);
                    break;
                case R.id.mature_stimulus_btn:
                    requestDirectory(MATURE_STIMULUS_REQUESTCODE);
                    break;
                case R.id.scrambled_mature_stimulus_btn:
                    requestDirectory(SCRAMBLED_MATURE_STIMULUS_REQUESTCODE);
                    break;
                case R.id.nonsocial_stimulus_btn:
                    requestDirectory(NONSOCIAL_STIMULUS_REQUESTCODE);
                    break;
                case R.id.social_training_stimulus_btn:
                    requestDirectory(SOCIAL_TRAINING_STIMULUS_REQUESTCODE);
                    break;
                case R.id.nonsocial_training_stimulus_btn:
                    requestDirectory(NONSOCIAL_TRAINING_STIMULUS_REQUESTCODE);
                    break;
                case R.id.familiarization_stimulus_btn:
                    requestDirectory(FAMILIARIZATION_STIMULUS_REQUESTCODE);
                    break;
            }
        }
    };

    private List<String> existingShortNames;

    private static class QueryShortNameAsyncTask extends AsyncTask<Void, Void, List<String>> {
        private NewCultureActivity newCultureActivity;

        private QueryShortNameAsyncTask(NewCultureActivity newCultureActivity) {
            this.newCultureActivity = newCultureActivity;
        }

        @Override
        protected List<String> doInBackground(Void... voids) {
            return ((ScRApplication) newCultureActivity.getApplication()).getAppDatabase()
                    .cultureDao().getAllShortNames();
        }

        @Override
        protected void onPostExecute(List<String> shortNames) {
            newCultureActivity.existingShortNames = shortNames;
        }
    }

    private static class StoreNewCultureAsyncTask extends AsyncTask<Culture, Void, Culture> {
        private NewCultureActivity newCultureActivity;

        private StoreNewCultureAsyncTask(NewCultureActivity newCultureActivity) {
            this.newCultureActivity = newCultureActivity;
        }

        @Override
        protected Culture doInBackground(Culture... cultures) {
            try {
                Culture culture = cultures[0];
                ((ScRApplication) newCultureActivity.getApplication()).getAppDatabase()
                        .cultureDao().insertNewCulture(culture);
                return culture;
            } catch (Exception e) {
                cancel(true);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Culture culture) {
            Intent data = new Intent();
            data.putExtra(SHORT_NAME, culture.getShortName());
            newCultureActivity.setResult(RESULT_OK, data);
            newCultureActivity.finish();
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(newCultureActivity, R.string.could_not_store_culture, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_culture_layout);

        new QueryShortNameAsyncTask(this).execute();

        findViewById(R.id.save_culture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trySaveNewCulture();
            }
        });

        findViewById(R.id.youth_stimulus_btn).setOnClickListener(directoryListener);
        findViewById(R.id.scrambled_youth_stimulus_btn).setOnClickListener(directoryListener);
        findViewById(R.id.mature_stimulus_btn).setOnClickListener(directoryListener);
        findViewById(R.id.scrambled_mature_stimulus_btn).setOnClickListener(directoryListener);
        findViewById(R.id.nonsocial_stimulus_btn).setOnClickListener(directoryListener);
        findViewById(R.id.social_training_stimulus_btn).setOnClickListener(directoryListener);
        findViewById(R.id.nonsocial_training_stimulus_btn).setOnClickListener(directoryListener);
        findViewById(R.id.familiarization_stimulus_btn).setOnClickListener(directoryListener);
    }

    private void trySaveNewCulture() {
        String shortName = getStringFromTextView(R.id.shortname);
        if (shortName.isEmpty()) {
            showMissingInfoToast(R.string.short_name_caption);
            return;
        }

        if (existingShortNames.contains(shortName)) {
            Toast.makeText(this, "Short name already in use", Toast.LENGTH_SHORT).show();
            return;
        }

        String longName = getStringFromTextView(R.id.longname);
        if (longName.isEmpty()) {
            showMissingInfoToast(R.string.long_name_caption);
            return;
        }

        String youthStimulus = getStringFromTextView(R.id.youth_stimulus);
        if (youthStimulus.isEmpty()) {
            showMissingInfoToast(R.string.youth_pictures_stimulus_caption);
            return;
        }

        String scrambledYouthStimulus = getStringFromTextView(R.id.scrambled_youth_stimulus);
        if (scrambledYouthStimulus.isEmpty()) {
            showMissingInfoToast(R.string.scrambled_youth_pictures_stimulus_caption);
            return;
        }

        String matureStimulus = getStringFromTextView(R.id.mature_stimulus);
        if (matureStimulus.isEmpty()) {
            showMissingInfoToast(R.string.mature_pictures_stimulus_caption);
            return;
        }

        String scrambledMatureStimulus = getStringFromTextView(R.id.scrambled_mature_stimulus);
        if (scrambledMatureStimulus.isEmpty()) {
            showMissingInfoToast(R.string.scrambled_mature_pictures_stimulus_caption);
            return;
        }

        String nonsocialStimulus = getStringFromTextView(R.id.nonsocial_stimulus);
        if (nonsocialStimulus.isEmpty()) {
            showMissingInfoToast(R.string.nonsocial_stimulus_caption);
            return;
        }

        String socialTrainingStimulus = getStringFromTextView(R.id.social_training_stimulus);
        if (socialTrainingStimulus.isEmpty()) {
            showMissingInfoToast(R.string.social_training_stimulus_caption);
            return;
        }

        String nonSocialTrainingStimulus = getStringFromTextView(R.id.nonsocial_training_stimulus);
        if (nonSocialTrainingStimulus.isEmpty()) {
            showMissingInfoToast(R.string.non_social_training_stimulus_caption);
            return;


        }
        String familiarizationStimulus = getStringFromTextView(R.id.familiarization_stimulus);
        if (familiarizationStimulus.isEmpty()) {
            showMissingInfoToast(R.string.familiarization_stimulus_caption);
            return;
        }

        new StoreNewCultureAsyncTask(this).execute(
                new Culture(shortName, longName,
                        youthStimulus, scrambledMatureStimulus,
                        matureStimulus, scrambledMatureStimulus,
                        nonsocialStimulus,
                        socialTrainingStimulus, nonSocialTrainingStimulus,
                        familiarizationStimulus));
    }

    private void showMissingInfoToast(int captionResId) {
        String info = getString(captionResId).replace(":", "");
        Toast.makeText(this, getString(R.string.missing_arg, info), Toast.LENGTH_SHORT).show();
    }

    @NonNull
    private String getStringFromTextView(int resId) {
        return ((TextView) findViewById(resId)).getText().toString().trim();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPermissions();
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_GRANT_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_GRANT_CODE:
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, R.string.cant_select_images_without_permissions, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
                break;
        }
    }


    private void requestDirectory(int requestCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(MIMETYPE_FILE);
        startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case YOUTH_STIMULUS_REQUESTCODE:
                    ((TextView) findViewById(R.id.youth_stimulus)).setText(getFile(data, false));
                    break;
                case SCRAMBLED_YOUTH_STIMULUS_REQUESTCODE:
                    ((TextView) findViewById(R.id.scrambled_youth_stimulus)).setText(getFile(data, false));
                    break;
                case MATURE_STIMULUS_REQUESTCODE:
                    ((TextView) findViewById(R.id.mature_stimulus)).setText(getFile(data, false));
                    break;
                case SCRAMBLED_MATURE_STIMULUS_REQUESTCODE:
                    ((TextView) findViewById(R.id.scrambled_mature_stimulus)).setText(getFile(data, false));
                    break;
                case NONSOCIAL_STIMULUS_REQUESTCODE:
                    ((TextView) findViewById(R.id.nonsocial_stimulus)).setText(getFile(data, false));
                    break;
                case SOCIAL_TRAINING_STIMULUS_REQUESTCODE:
                    ((TextView) findViewById(R.id.social_training_stimulus)).setText(getFile(data, false));
                    break;
                case NONSOCIAL_TRAINING_STIMULUS_REQUESTCODE:
                    ((TextView) findViewById(R.id.nonsocial_training_stimulus)).setText(getFile(data, false));
                    break;
                case FAMILIARIZATION_STIMULUS_REQUESTCODE:
                    ((TextView) findViewById(R.id.familiarization_stimulus)).setText(getFile(data, false));
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String getFile(Intent data, boolean directory) {
        String dataString = data.getDataString();

        if (dataString != null) {
            String scheme = Uri.parse(dataString).getScheme();
            File file;
            if ("content".equals(scheme)) {
                String path = UriUtils.getPath(this, dataString);
                if (path == null) {
                    Toast.makeText(this, getString(R.string.cant_handle_path_string_arg, dataString), Toast.LENGTH_SHORT).show();
                    return null;
                }
                file = new File(path);
            } else if ("file".equals(scheme)) {
                file = new File(dataString.substring((scheme + "://").length()));
            } else {
                Toast.makeText(this, getString(R.string.cant_handle_file_schema_arg, scheme), Toast.LENGTH_SHORT).show();
                return null;
            }

            if (directory) {
                file = file.getParentFile();
            }
            return Uri.fromFile(file).toString();
        } else {
            return null;
        }
    }
}
