package de.uni_leipzig.lfe.scr_tab.socialreward.block;

public class TrialRecord {
    private Side side;
    private long duration;
    private long siTime;
    private long isiTime;
    private StimulusType stimulusType;

    public void setSide(Side side) {
        this.side = side;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public void setSiTime(long siTime) {
        this.siTime = siTime;
    }

    public void setIsiTime(long isiTime) {
        this.isiTime = isiTime;
    }

    public void setStimulusType(StimulusType stimulusType) {
        this.stimulusType = stimulusType;
    }

    public Side getSide() {
        return side;
    }

    public long getDuration() {
        return duration;
    }

    public long getSiTime() {
        return siTime;
    }

    public long getIsiTime() {
        return isiTime;
    }

    public StimulusType getStimulusType() {
        return stimulusType;
    }
}
