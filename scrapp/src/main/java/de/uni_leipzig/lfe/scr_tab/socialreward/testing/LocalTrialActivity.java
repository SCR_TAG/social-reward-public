package de.uni_leipzig.lfe.scr_tab.socialreward.testing;

import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Queue;

import de.uni_leipzig.lfe.scr_tab.socialreward.R;
import de.uni_leipzig.lfe.scr_tab.socialreward.ScRApplication;
import de.uni_leipzig.lfe.scr_tab.socialreward.block.BlockPlanner;
import de.uni_leipzig.lfe.scr_tab.socialreward.block.Side;
import de.uni_leipzig.lfe.scr_tab.socialreward.block.Trial;
import de.uni_leipzig.lfe.scr_tab.socialreward.block.TrialConfiguration;
import de.uni_leipzig.lfe.scr_tab.socialreward.testing.view.SquareView;

public class LocalTrialActivity extends AppCompatActivity {

    private Queue<Trial> trials;
    private SquareView leftButton;
    private SquareView rightButton;
    private ImageView leftStimulusImageView;
    private ImageView rightStimulusImageView;
    private Trial currentTrial;
    private Handler handler = new Handler();
    private boolean stimulusClickAllowed = false;
    private BlockExecutionRecorder blockExecutionRecorder = new BlockExecutionRecorder();

    public class OnBlockRecordStoredListener {
        public void onBlockRecordStored(long id) {
            TextView recordInfoTextView = (TextView) findViewById(R.id.recordidinfo);
            recordInfoTextView.setText(getString(R.string.recordinfo, id));
            recordInfoTextView.setVisibility(View.VISIBLE);
        }
    }

    private View.OnClickListener controlButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (trials.isEmpty()) {
                finish();
            } else {
                setControlVisibility(View.GONE);
                setTestingVisibility(View.VISIBLE);
                clearStimuli();

                setNextTrial();
            }
        }
    };

    private Runnable interStimulusIntervalTriggerer = new Runnable() {
        @Override
        public void run() {
            setButtonsVisibility(View.GONE);
            clearStimuli();
        }
    };

    private Runnable nextTrialTriggerer = new Runnable() {
        @Override
        public void run() {
            setNextTrial();
        }
    };

    private Runnable startNextBlockPartTriggerer = new Runnable() {
        @Override
        public void run() {
            int resId = 0;
            switch (currentTrial.getFlag()) {
                case BlockPlanner.END_OF_FAMILIARIZATION:
                    resId = R.string.start_training;
                    break;
                case BlockPlanner.END_OF_TRAINING:
                    resId = R.string.start_testing;
                    break;
                case BlockPlanner.END_OF_TESTING:
                    resId = R.string.finished;
                    ((ScRApplication) getApplication()).setBlockExecutionRecord(blockExecutionRecorder,
                            new OnBlockRecordStoredListener());
                    break;
            }
            showControl(resId);
        }
    };

    private View.OnClickListener stimulusButtonClickListener = new View.OnClickListener() {
        @Override
        public synchronized void onClick(View v) {
            if (!stimulusClickAllowed) {
                return;
            }
            stimulusClickAllowed = false;

            setButtonsVisibility(View.GONE);

            ImageView stimulusView;
            Uri stimulusUri;
            if (v.getId() == R.id.left_stimulus_button) {
                blockExecutionRecorder.stop(currentTrial, Side.LEFT);

                stimulusView = leftStimulusImageView;
                stimulusUri = currentTrial.getLeft().getStimulusUri();
            } else {
                blockExecutionRecorder.stop(currentTrial, Side.RIGHT);

                stimulusView = rightStimulusImageView;
                stimulusUri = currentTrial.getRight().getStimulusUri();
            }
            showStimulusInView(stimulusView, stimulusUri);

            handler.postDelayed(interStimulusIntervalTriggerer, currentTrial.getSi());
            handler.postDelayed(currentTrial.getFlag() == 0
                            ? nextTrialTriggerer : startNextBlockPartTriggerer,
                    currentTrial.getSi() + currentTrial.getIsi());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_local_trial);

        leftButton = findViewById(R.id.left_stimulus_button);
        rightButton = findViewById(R.id.right_stimulus_button);
        leftStimulusImageView = findViewById(R.id.left_stimulus_imageView);
        rightStimulusImageView = findViewById(R.id.right_stimulus_imageView);

        findViewById(R.id.control_btn).setOnClickListener(controlButtonClickListener);
        leftButton.setOnClickListener(stimulusButtonClickListener);
        rightButton.setOnClickListener(stimulusButtonClickListener);

        trials = ((ScRApplication) getApplication()).getTrials();
    }

    private void showControl(int controlStringResId) {
        setControlVisibility(View.VISIBLE);
        ((Button) findViewById(R.id.control_btn)).setText(controlStringResId);
        setTestingVisibility(View.GONE);
    }

    private void setNextTrial() {
        stimulusClickAllowed = true;
        currentTrial = trials.poll();
        setTrialButtons(currentTrial);
        blockExecutionRecorder.start(currentTrial);
    }

    private void clearStimuli() {
        leftStimulusImageView.setVisibility(View.GONE);
        rightStimulusImageView.setVisibility(View.GONE);
    }

    private void setButtonsVisibility(int visibility) {
        leftButton.setVisibility(visibility);
        rightButton.setVisibility(visibility);
    }

    private void showStimulusInView(ImageView stimulusView, Uri stimulusUri) {
        stimulusView.setVisibility(View.VISIBLE);
        stimulusView.setImageURI(stimulusUri);
    }

    private void setTrialButtons(Trial trial) {
        showColorOnButton(trial.getLeft(), leftButton);
        showColorOnButton(trial.getRight(), rightButton);
    }

    private void showColorOnButton(TrialConfiguration trialConfiguration, SquareView button) {
        if (trialConfiguration == null) {
            button.setVisibility(View.GONE);
        } else {
            button.setVisibility(View.VISIBLE);
            setViewBackgroundTint(button, getColor(trialConfiguration.getColorId()));
        }
    }

    private void setControlVisibility(int visibility) {
        findViewById(R.id.control_btn).setVisibility(visibility);
    }

    private void setTestingVisibility(int visibility) {
        findViewById(R.id.left_stimulus_layout).setVisibility(visibility);
        // findViewById(R.id.stimulus_divider).setVisibility(visibility);
        findViewById(R.id.right_stimulus_layout).setVisibility(visibility);
    }

    private void setViewBackgroundTint(View view, int color) {
        ColorStateList myList = createColorStateList(color);
        view.setBackgroundTintList(myList);
    }

    @NonNull
    private ColorStateList createColorStateList(int color) {
        int[][] states = new int[][]{
                new int[]{android.R.attr.state_enabled}, // enabled
                new int[]{-android.R.attr.state_enabled}, // disabled
                new int[]{-android.R.attr.state_checked}, // unchecked
                new int[]{android.R.attr.state_pressed}  // pressed
        };
        int[] colorArray = new int[states.length];
        Arrays.fill(colorArray, color);
        return new ColorStateList(states, colorArray);
    }
}

