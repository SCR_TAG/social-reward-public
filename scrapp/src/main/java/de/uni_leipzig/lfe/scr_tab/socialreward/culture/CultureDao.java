package de.uni_leipzig.lfe.scr_tab.socialreward.culture;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.database.Cursor;

import java.util.List;

@Dao
public interface CultureDao {
    @Insert
    void insertNewCulture(Culture culture);

    @Query("SELECT shortName FROM culture")
    List<String> getAllShortNames();

    @Query("SELECT * FROM culture")
    Cursor getAllCultures();

    @Query("SELECT * FROM culture WHERE _id=:cultureId")
    Culture getCultureById(long cultureId);
}
