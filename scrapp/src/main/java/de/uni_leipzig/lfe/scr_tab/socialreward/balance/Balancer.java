package de.uni_leipzig.lfe.scr_tab.socialreward.balance;

public interface Balancer<T> {
    T getNext();
    boolean hasNext();
}
