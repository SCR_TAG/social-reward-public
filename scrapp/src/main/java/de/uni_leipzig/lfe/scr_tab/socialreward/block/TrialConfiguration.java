package de.uni_leipzig.lfe.scr_tab.socialreward.block;

import android.net.Uri;

public class TrialConfiguration {

    private StimulusType stimulusType;
    private int colorId;
    private Uri stimulusUri;

    TrialConfiguration(StimulusType stimulusType, int colorId, Uri stimulusUri) {
        this.stimulusType = stimulusType;
        this.colorId = colorId;
        this.stimulusUri = stimulusUri;
    }

    public int getColorId() {
        return colorId;
    }

    public Uri getStimulusUri() {
        return stimulusUri;
    }

    public StimulusType getStimulusType() {
        return stimulusType;
    }
}
