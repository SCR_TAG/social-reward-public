package de.uni_leipzig.lfe.scr_tab.socialreward.block;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface BlockRecordDao {
    @Insert
    long insertBlockRecord(BlockRecord blockRecord);

    @Query("SELECT * FROM blockrecord")
    List<BlockRecord> getAllRecords();

    @Query("SELECT * FROM blockrecord WHERE culture=:culture")
    List<BlockRecord> getRecordsByCulture(String culture);
}
