package de.uni_leipzig.lfe.scr_tab.socialreward.configurationmode;

public enum ConfigurationMode {
    CONFIGURATOR,
    FOLLOWER
}
