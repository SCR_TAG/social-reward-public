package de.uni_leipzig.lfe.scr_tab.socialreward;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.util.Pair;

import java.util.Queue;

import de.uni_leipzig.lfe.scr_tab.socialreward.block.BlockExecutionPersistenceTask;
import de.uni_leipzig.lfe.scr_tab.socialreward.block.BlockPlan;
import de.uni_leipzig.lfe.scr_tab.socialreward.block.Trial;
import de.uni_leipzig.lfe.scr_tab.socialreward.testing.BlockExecutionRecorder;
import de.uni_leipzig.lfe.scr_tab.socialreward.testing.LocalTrialActivity;

public class ScRApplication extends Application {

    private AppDatabase appDatabase;
    private BlockPlan blockPlan;

    public AppDatabase getAppDatabase() {
        if (appDatabase == null) {
            appDatabase = Room.databaseBuilder(this, AppDatabase.class, "socialreward-db").build();
        }
        return appDatabase;
    }

    public Queue<Trial> getTrials() {
        return blockPlan.getTrials();
    }

    public void setBlockPlan(BlockPlan blockPlan) {
        this.blockPlan = blockPlan;
    }

    public void setBlockExecutionRecord(BlockExecutionRecorder blockExecutionRecorder,
                                        LocalTrialActivity.OnBlockRecordStoredListener onBlockRecordStoredListener) {
        new BlockExecutionPersistenceTask(this, onBlockRecordStoredListener)
                .execute(Pair.create(blockPlan, blockExecutionRecorder));
    }
}
