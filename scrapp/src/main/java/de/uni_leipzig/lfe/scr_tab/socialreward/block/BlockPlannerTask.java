package de.uni_leipzig.lfe.scr_tab.socialreward.block;

import android.content.Intent;
import android.os.AsyncTask;

import java.util.Queue;

import de.uni_leipzig.lfe.scr_tab.socialreward.ScRApplication;
import de.uni_leipzig.lfe.scr_tab.socialreward.blockconfig.BlockConfiguration;
import de.uni_leipzig.lfe.scr_tab.socialreward.testing.LocalTrialActivity;

public class BlockPlannerTask extends AsyncTask<BlockConfiguration,Void, BlockPlan> {
    private ScRApplication application;

    public BlockPlannerTask(ScRApplication application) {
        this.application = application;
    }

    @Override
    protected BlockPlan doInBackground(BlockConfiguration... blockConfigurations) {
        return new BlockPlanner().createBlockPlan(application, blockConfigurations[0]);
    }

    @Override
    protected void onPostExecute(BlockPlan blockPlan) {
        application.setBlockPlan(blockPlan);

        Intent intent = new Intent(application, LocalTrialActivity.class);
        application.startActivity(intent);
    }
}
