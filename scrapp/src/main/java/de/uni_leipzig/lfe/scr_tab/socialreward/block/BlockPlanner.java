package de.uni_leipzig.lfe.scr_tab.socialreward.block;

import android.net.Uri;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

import de.uni_leipzig.lfe.scr_tab.socialreward.R;
import de.uni_leipzig.lfe.scr_tab.socialreward.ScRApplication;
import de.uni_leipzig.lfe.scr_tab.socialreward.balance.BalanceFactory;
import de.uni_leipzig.lfe.scr_tab.socialreward.balance.Balancer;
import de.uni_leipzig.lfe.scr_tab.socialreward.blockconfig.BlockConfiguration;
import de.uni_leipzig.lfe.scr_tab.socialreward.culture.Culture;

/**
 * Ein Ablaufplan soll erzeugt werden, bevor begonnen wird.<p/>
 * <b>"Familiarization" (Lerndurchgang)</b><br>
 * Weißer Kreis links -> Bild (konstantes Bild vom Auto)<br>
 * Weißer Kreis rechts -> Bild<br>
 * Weißer Kreis links -> Bild<br>
 * Weißer Kreis rechts -> Bild<p>
 * <b>"Training"</b><br>
 * Counterbalanced über alle Probanden einer Kultur: Farbe->Stimulus, Farbabfolge<br>
 * Rot links -> Stimulus A<br>
 * Blau links -> Stimulus B<br>
 * Rot rechts -> Stimulus A<br>
 * Blau rechts -> Stimulus B<p>
 * <b>"Test"</b><br>
 * 8 Trials, counterbalanced 4 / 4<br>
 * Stimulus Interval (SI, Darstellung des Rewards) randomisiert [2,4] Sekunden: Scha<br>
 * InterStimulusInterval (ISI) randomisiert [2,4] Sekunden<br>
 */
public class BlockPlanner {

    private static final long TRAINING_STIMULUS_INTERVAL = 2000;
    private static final long TRAINING_INTER_STIMULUS_INTERVAL = 4000;

    private static final long MIN_STIMULUS_INTERVAL = 2000;
    private static final long MAX_STIMULUS_INTERVAL = 4000;
    private static final long STIMULUS_INTERVAL_VARIANCE = MAX_STIMULUS_INTERVAL - MIN_STIMULUS_INTERVAL;

    private static final long MIN_INTER_STIMULUS_INTERVAL = 2000;
    private static final long MAX_INTER_STIMULUS_INTERVAL = 4000;
    private static final long INTER_STIMULUS_INTERVAL_VARIANCE = MAX_INTER_STIMULUS_INTERVAL - MIN_INTER_STIMULUS_INTERVAL;

    public static final int END_OF_FAMILIARIZATION = -1;
    public static final int END_OF_TRAINING = -2;
    public static final int END_OF_TESTING = -3;

    private static final int TEST_BLOCK_SIZE = 8;
    private Random rnd = new Random();

    public BlockPlan createBlockPlan(ScRApplication application, BlockConfiguration blockConfiguration) {
        Queue<Trial> trials = new LinkedList<>();

        Culture culture = application.getAppDatabase().cultureDao().getCultureById(blockConfiguration.getCultureId());

        trials.addAll(createFamiliarizationBlock(culture));
        trials.addAll(createTrainingBlock(culture, blockConfiguration));
        trials.addAll(createTestBlock(culture, blockConfiguration));

        return new BlockPlan(blockConfiguration, trials);
    }

    private Collection<? extends Trial> createFamiliarizationBlock(Culture culture) {
        List<Trial> trials = new ArrayList<>();

        int whiteId = android.R.color.white;
        Uri drawableUri = Uri.parse(culture.getFamiliarizationStimulus());

        long si = TRAINING_STIMULUS_INTERVAL;
        long isi = TRAINING_INTER_STIMULUS_INTERVAL;

        trials.add(new Trial(new TrialConfiguration(StimulusType.NONE, whiteId, drawableUri), null, si, isi));
        trials.add(new Trial(null, new TrialConfiguration(StimulusType.NONE, whiteId, drawableUri), si, isi));
        trials.add(new Trial(new TrialConfiguration(StimulusType.NONE, whiteId, drawableUri), null, si, isi));
        trials.add(new Trial(null, new TrialConfiguration(StimulusType.NONE, whiteId, drawableUri), si, isi, END_OF_FAMILIARIZATION));

        return trials;
    }


    private Collection<? extends Trial> createTrainingBlock(Culture culture, BlockConfiguration blockConfiguration) {
        List<Trial> trials = new ArrayList<>();

        Uri socTrainStimulus = Uri.parse(culture.getSocialTrainingStimulus());
        Uri nonSocTrainStimulus = Uri.parse(culture.getNonSocialTrainingStimulus());

        int socStimColorId = blockConfiguration.getSocialStimuliColorId();
        int nonSocStimColorId = getNonSocialStimulusColor(socStimColorId);

        long si = TRAINING_STIMULUS_INTERVAL;
        long isi = TRAINING_INTER_STIMULUS_INTERVAL;

        trials.add(new Trial(new TrialConfiguration(StimulusType.SOCIAL, socStimColorId, socTrainStimulus), null, si, isi));
        trials.add(new Trial(new TrialConfiguration(StimulusType.NONSOCIAL, nonSocStimColorId, nonSocTrainStimulus), null, si, isi));
        trials.add(new Trial(null, new TrialConfiguration(StimulusType.SOCIAL, socStimColorId, socTrainStimulus), si, isi));
        trials.add(new Trial(null, new TrialConfiguration(StimulusType.NONSOCIAL, nonSocStimColorId, nonSocTrainStimulus), si, isi, END_OF_TRAINING));

        return trials;
    }

    private Collection<? extends Trial> createTestBlock(Culture culture, BlockConfiguration blockConfiguration) {
        List<Trial> trials = new ArrayList<>();

        Balancer<Side> balancer = new BalanceFactory<Side>().createBalancer(Arrays.asList(Side.values()), TEST_BLOCK_SIZE);

        boolean useScrambledPictures = blockConfiguration.isScrambleImages();
        Uri socialStimulus = Uri.parse(blockConfiguration.showMatureImages()
                ? (useScrambledPictures ? culture.getScrambledMaturePictureStimulus() : culture.getMaturePictureStimulus())
                : (useScrambledPictures ? culture.getScrambledYouthPictureStimulus() : culture.getYouthPictureStimulus()));
        Uri nonSocialStimulus = Uri.parse(culture.getNonsocialStimulus());

        Trial currentTrial = null;

        while (balancer.hasNext()) {
            int socStimColorId = blockConfiguration.getSocialStimuliColorId();
            int nonSocStimColorId = getNonSocialStimulusColor(socStimColorId);

            TrialConfiguration socialStimulusTrial = new TrialConfiguration(StimulusType.SOCIAL, socStimColorId, socialStimulus);
            TrialConfiguration nonSocialStimulusTrial = new TrialConfiguration(StimulusType.NONSOCIAL, nonSocStimColorId, nonSocialStimulus);

            TrialConfiguration lTrialConf;
            TrialConfiguration rTrialConf;
            if (balancer.getNext() == Side.LEFT) {
                lTrialConf = socialStimulusTrial;
                rTrialConf = nonSocialStimulusTrial;
            } else {
                lTrialConf = nonSocialStimulusTrial;
                rTrialConf = socialStimulusTrial;
            }

            long si = MIN_STIMULUS_INTERVAL + rnd.nextInt((int) STIMULUS_INTERVAL_VARIANCE);
            long isi = MIN_INTER_STIMULUS_INTERVAL + rnd.nextInt((int) INTER_STIMULUS_INTERVAL_VARIANCE);

            currentTrial = new Trial(lTrialConf, rTrialConf, si, isi);
            trials.add(currentTrial);
        }

        if (currentTrial != null) {
            currentTrial.setFlag(END_OF_TESTING);
        }
        return trials;
    }

    private int getNonSocialStimulusColor(int socialStimulusColor) {
        return socialStimulusColor == R.color.socialStimulusColor1
                ? R.color.socialStimulusColor2
                : R.color.socialStimulusColor1;
    }
}
