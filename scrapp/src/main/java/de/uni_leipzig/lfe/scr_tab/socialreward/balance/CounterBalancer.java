package de.uni_leipzig.lfe.scr_tab.socialreward.balance;

import java.util.List;
import java.util.Random;

public class CounterBalancer<T> implements Balancer<T> {
    /**
     * Holds the list of all possible values. Here only size = 2 allowed!
     */
    private List<T> values;

    private final int valuesSize;

    /**
     * Holds how many times a value has to be returned.
     */
    private final int totalCount;

    /**
     * Holds how many values have been returned.
     */
    private int position = 0;

    /**
     * Holds the balance and indicates when one value has been returned too many times in a row.
     */
    private int balance = 0;

    /**
     * Keeps score and forces one value to be given if the counterbalancing requires only one value to be returned.
     */
    private int counter = 0;

    private Random rnd;

    CounterBalancer(List<T> values, int totalCount, Random rnd) {
        this.values = values;
        valuesSize = values.size();
        this.totalCount = totalCount;
        this.rnd = rnd;
    }

    public T getNext() {
        try {
            int idx;
            int r = rnd.nextInt(valuesSize);

            // if counterbalancing allows for free choice
            if (Math.abs(counter) < (totalCount - position)) {

                // if the balance allows for free choice
                if (-1 <= balance && balance <= 1) {
                    idx = r;
                    int locWeight = 1 - (2 * r);

                    // update balance
                    balance += locWeight;
                } else {
                    // re-balance!
                    idx = Math.max(0, balance) / 2;

                    // set balance
                    balance = -1 * (int) Math.signum(balance);
                }

            } else {
                // counterbalance requires to return only one value for ever ...
                idx = counter > 0 ? 1 : 0;
            }

            // update score
            counter += (1 - 2 * idx);

            return values.get(idx);
        } finally {
            position++;
        }
    }

    public boolean hasNext() {
        return position < totalCount;
    }
}
