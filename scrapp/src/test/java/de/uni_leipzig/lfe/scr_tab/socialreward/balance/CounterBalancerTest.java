package de.uni_leipzig.lfe.scr_tab.socialreward.balance;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class CounterBalancerTest {

    @Test
    public void getNext_RandomOnlyZeros() {
        Random rnd = Mockito.mock(Random.class);
        Mockito.when(rnd.nextInt(2)).thenReturn(0, 0, 0, 0, 0, 0, 0, 0);

        CounterBalancer<String> counterBalancer = new CounterBalancer<>(Arrays.asList("a", "b"), 8, rnd);

        assertEquals("a", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
    }

    @Test
    public void getNext_RandomOnlyOnes() {
        Random rnd = Mockito.mock(Random.class);
        Mockito.when(rnd.nextInt(2)).thenReturn(1, 1, 1, 1, 1, 1, 1, 1);

        CounterBalancer<String> counterBalancer = new CounterBalancer<>(Arrays.asList("a", "b"), 8, rnd);

        assertEquals("b", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
    }

    @Test
    public void getNext_RandomAlternating() {
        Random rnd = Mockito.mock(Random.class);
        Mockito.when(rnd.nextInt(2)).thenReturn(1, 0, 1, 0, 1, 0, 1, 0);

        CounterBalancer<String> counterBalancer = new CounterBalancer<>(Arrays.asList("a", "b"), 8, rnd);

        assertEquals("b", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
    }

    @Test
    public void getNext_RandomFirstAllZerosThenAllOnes() {
        Random rnd = Mockito.mock(Random.class);
        Mockito.when(rnd.nextInt(2)).thenReturn(0, 0, 0, 0, 1, 1, 1, 1);

        CounterBalancer<String> counterBalancer = new CounterBalancer<>(Arrays.asList("a", "b"), 8, rnd);

        assertEquals("a", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
    }

    @Test
    public void getNext_RandomPairwiseZeroOne() {
        Random rnd = Mockito.mock(Random.class);
        Mockito.when(rnd.nextInt(2)).thenReturn(0, 0, 1, 1, 0, 0, 1, 1);

        CounterBalancer<String> counterBalancer = new CounterBalancer<>(Arrays.asList("a", "b"), 8, rnd);

        assertEquals("a", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("a", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
        assertEquals("b", counterBalancer.getNext());
    }

    @Test
    public void hasNext_TrueForTotalCountThenFalse() {
        CounterBalancer<String> counterBalancer = new CounterBalancer<>(Arrays.asList("a", "b"), 8, new Random());

        counterBalancer.getNext();
        assertEquals(true, counterBalancer.hasNext());
        counterBalancer.getNext();
        assertEquals(true, counterBalancer.hasNext());
        counterBalancer.getNext();
        assertEquals(true, counterBalancer.hasNext());
        counterBalancer.getNext();
        assertEquals(true, counterBalancer.hasNext());
        counterBalancer.getNext();
        assertEquals(true, counterBalancer.hasNext());
        counterBalancer.getNext();
        assertEquals(true, counterBalancer.hasNext());
        counterBalancer.getNext();
        assertEquals(true, counterBalancer.hasNext());
        counterBalancer.getNext();
        assertEquals(false, counterBalancer.hasNext());
        assertEquals(false, counterBalancer.hasNext());
    }
}
